import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, App, LoadingController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { PodcastPage } from '../pages/podcast/podcast';
import { BiblePassagePage } from '../pages/bible-passage/bible-passage';
import { SermonsPage } from '../pages/sermons/sermons';
import { BulletinsPage } from '../pages/bulletins/bulletins';
import { PledgesPage } from '../pages/pledges/pledges';
import { DonationsPage } from '../pages/donations/donations';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { ModelService } from '../providers/model.service';
import { MyPledgesPage } from '../pages/my-pledges/my-pledges';
import { AboutPage } from '../pages/about/about';
import { EditAccountPage } from '../pages/edit-account/edit-account';
import { LiveStreamingPage } from '../pages/live-streaming/live-streaming';
import { GalleryPage } from '../pages/gallery/gallery';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  pageLogin:any;
  pageRegister:any;
  pageMyPledges:any;
  pageEditAccount: typeof EditAccountPage;

  user_email: any;
  user_name: any;
  auth: boolean;
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{icon: string, title: string, component: any}>;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public modelService: ModelService,
    public appCtrl: App,
    public loadingCtrl: LoadingController,
    public events: Events) {
    this.initializeApp();

    this.pageLogin = LoginPage;
    this.pageRegister = RegisterPage;
    this.pageMyPledges = MyPledgesPage;
    this.pageEditAccount = EditAccountPage;

    this.auth = modelService.auth();
    if (modelService.auth() === true) {
      this.set_login();
    }

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'home', title: 'Home', component: HomePage },
      { icon: 'headset', title: 'Podcast', component: PodcastPage },
      { icon: 'book', title: 'Daily Bible Passage', component: BiblePassagePage },
      { icon: 'logo-youtube', title: 'Sermons', component: SermonsPage },
      { icon: 'list-box', title: 'Bulletins', component: BulletinsPage },
      { icon: 'heart', title: 'E-Giving', component: DonationsPage },
      { icon: 'hand', title: 'Make A Pledge', component: PledgesPage },
      { icon: 'desktop', title: 'Live Streaming', component: LiveStreamingPage },
      { icon: 'information-circle', title: 'About The Church', component: AboutPage },
      { icon: 'images', title: 'Photo Gallery', component: GalleryPage }
    ];

    events.subscribe('user:login', (userData) => {
      this.set_login();
    });


    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  loadPage(page) {
    this.nav.setRoot(page);
  }

  pushPage(page){
    this.nav.push(page);
  }

  set_login(){
    this.user_name = window.localStorage.getItem("Name");
    this.user_email = window.localStorage.getItem("Email");
    this.auth = true;
  }
  

  logout() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    setTimeout(() => {
      this.modelService.logoutSet();
      this.auth = false;
      loading.dismiss();
      this.appCtrl.getRootNav().setRoot(HomePage);
    }, 1000);
  }

}
