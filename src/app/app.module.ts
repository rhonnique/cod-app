import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

/* Providers */
import { ModelService } from '../providers/model.service';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { NgCalendarModule  } from 'ionic2-calendar';
import { StreamingMedia } from '@ionic-native/streaming-media';
import { SermonsPage } from '../pages/sermons/sermons';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
//import { CUSTOM_ELEMENTS_SCHEMA } from 'ionic-audio/node_modules/@angular/core';

import { IonicAudioModule, AudioProvider, WebAudioProvider, audioProviderFactory } from 'ionic-audio';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { VideoPlayer } from '@ionic-native/video-player';
import { YoutubeServiceProvider } from '../providers/youtube-service/youtube-service';
import { PlayerProvider } from '../providers/player/player';
import { YtProvider } from '../providers/yt/yt';
import { MomentPipe } from '../pipes/moment/moment';

import { HomePage } from '../pages/home/home';
import { BulletinsPage } from '../pages/bulletins/bulletins';
import { PledgesPage } from '../pages/pledges/pledges';
import { DonationsPage } from '../pages/donations/donations';
import { PledgeDetailsPage } from '../pages/pledge-details/pledge-details';
import { PodcastDetailsPage } from '../pages/podcast-details/podcast-details';
import { PodcastPage } from '../pages/podcast/podcast';
import { BiblePassagePage } from '../pages/bible-passage/bible-passage';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { MyPledgesPage } from '../pages/my-pledges/my-pledges';
import { MyPledgeDetailsPage } from '../pages/my-pledge-details/my-pledge-details';
import { AboutPage } from '../pages/about/about';
import { EditAccountPage } from '../pages/edit-account/edit-account';
import { AboutVisionPage } from '../pages/about-vision/about-vision';
import { AboutContactPage } from '../pages/about-contact/about-contact';
import { AboutServicesPage } from '../pages/about-services/about-services';
import { AboutStatementPage } from '../pages/about-statement/about-statement';
import { LiveStreamingPage } from '../pages/live-streaming/live-streaming';
import { SermonDetailsPage } from '../pages/sermon-details/sermon-details';
import { GalleryPage } from '../pages/gallery/gallery';
import { GalleryDetailsPage } from '../pages/gallery-details/gallery-details';


/**
 * Sample custom factory function to use with ionic-audio
 */
export function myCustomAudioProviderFactory() {
  return new WebAudioProvider();
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AboutPage,
    PodcastPage,
    BiblePassagePage,
    SermonsPage,
    BulletinsPage,
    PledgesPage,
    DonationsPage,
    PledgeDetailsPage,
    PodcastDetailsPage,
    LoginPage,
    RegisterPage,
    MyPledgesPage,
    MyPledgeDetailsPage,
    EditAccountPage,
    AboutServicesPage,
    AboutStatementPage,
    AboutContactPage,
    AboutVisionPage,
    LiveStreamingPage,
    SermonDetailsPage,
    MomentPipe,
    GalleryPage,
    GalleryDetailsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicAudioModule.forRoot({ provide: AudioProvider, useFactory: audioProviderFactory }), 
    NgCalendarModule,
    SuperTabsModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AboutPage,
    PodcastPage,
    BiblePassagePage,
    SermonsPage,
    BulletinsPage,
    PledgesPage,
    DonationsPage,
    PledgeDetailsPage,
    PodcastDetailsPage,
    LoginPage,
    RegisterPage,
    MyPledgesPage,
    MyPledgeDetailsPage,
    EditAccountPage,
    AboutServicesPage,
    AboutStatementPage,
    AboutContactPage,
    AboutVisionPage,
    LiveStreamingPage,
    SermonDetailsPage,
    GalleryPage,
    GalleryDetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: Window, useValue: window},
    StreamingMedia,
    ModelService,
    YoutubeVideoPlayer,
    VideoPlayer,
    YoutubeServiceProvider,
    PlayerProvider,
    YtProvider
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
