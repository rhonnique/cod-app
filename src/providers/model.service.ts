import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
//import { Observable } from "rxjs/Observable";
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { AlertController, Platform, ToastController } from "ionic-angular";
import { Network } from '@ionic-native/network';
declare var navigator: any;


/*
  Generated class for the Model provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ModelService {

  public SiteUrl = "https://www.lffyouthsortingout.com/cod";
  public Api = "https://www.lffyouthsortingout.com/cod/api";
  //public SiteUrl: string = "http://localhost:8888/church-server";
  //public Api: string = "http://localhost:8888/church-server/api";
  EventData: any;
  public Network_Status: boolean;

  constructor(
    public http: Http,
    private alertCtrl: AlertController,
    private platform: Platform,
    private network: Network,
    public toastCtrl: ToastController
  ) {
    platform.ready().then(() => {
      this.Network_Status = navigator.onLine;
    });

    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.Network_Status = false;
    });

    let connectSubscription = this.network.onConnect().subscribe(() => {
      this.Network_Status = true;
    });
  }


  public handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(error, 'status error');
    return Observable.throw(error);
  }


  showToast(msg: any, position: string = "bottom") {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: position
    });

    toast.present(toast);
  }

  sermons() {
    let url: string = this.Api + "/sermons";
    return this.http.get(url)
      .map(res => res.json())
      //.do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  sermon_details(sermonID) {
    let url: string = this.Api + "/sermon-details/" + sermonID;
    return this.http.get(url)
      .map(res => res.json())
      //.do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  pledges() {
    let url: string = this.Api + "/pledges";
    return this.http.get(url)
      .map(res => res.json())
      //.do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  pledge_details(data: any) {
    let url: string = this.Api + "/pledge-details";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  make_pledge(data: any) {
    let url: string = this.Api + "/make-a-pledge";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  my_pledges(member_id) {
    let url: string = this.Api + "/member-pledges/" + member_id;
    // console.log(url);
    return this.http.get(url)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  pledge_summary(data: any) {
    let url: string = this.Api + "/pledge-summary";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  pledge_done(data: any) {
    let url: string = this.Api + "/pledge-done";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }


  my_pledge_details(member_id, pledge_id) {
    let url: string = this.Api + "/member-pledge-details/" + member_id + '/' + pledge_id;
    return this.http.get(url)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }

  daily_passage() {
    // https://beta.ourmanna.com/api/v1/get/?format=json
    let url: string = 'https://beta.ourmanna.com/api/v1/get/?format=json';
    return this.http.get(url)
      .map(res => res.json())
      // .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }



  bulletin() {
    let url: string = this.Api + "/bulletin";
    return this.http.get(url)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch(this.handleError);
  }



  podcast() {
    let url: string = this.Api + "/podcast";
    return this.http.get(url)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  gallery() {
    let url: string = this.Api + "/gallery";
    return this.http.get(url)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  gallery_details(ID) {
    let url: string = this.Api + "/gallery-details/" + ID;
    return this.http.get(url)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  event() {
    let url: string = this.Api + "/current-event";
    let events;
    this.http.get(url)
      .map(res => res.json())
      //.do(data => console.log("All: " + JSON.stringify(data)))
      .subscribe(event => events);
    return events;
  }

  states() {
    let url: string = this.Api + "/states.json";
    return this.http.get(url)
      .map(res => res.json())
      //.do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  rooms() {
    let url: string = this.Api + "/rooms";
    return this.http.get(url)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  check_register(data: any) {
    let url: string = this.Api + "/register-check";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    //headers.append('Access-Control-Allow-Credentials', 'true' );
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  login(data: any) {
    let url: string = this.Api + "/login";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    //headers.append('Access-Control-Allow-Credentials', 'true' );
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  register(data: any, stage: number = 0) {
    let url: string;
    url = this.Api + "/register";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  edit_account(data: any) {
    let url: string = this.Api + "/edit-account";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  donations_summary(data: any) {
    let url: string = this.Api + "/donations-summary";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    //headers.append('Access-Control-Allow-Credentials', 'true' );
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  donations_done(data: any) {
    let url: string = this.Api + "/donations-done";
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    //headers.append('Access-Control-Allow-Credentials', 'true' );
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(data), options)
      .map(res => res.json())
      .do(data => console.log("All: " + JSON.stringify(data)))
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  dfAlert(data: any, setFocus = null) {
    let alert = this.alertCtrl.create({
      title: data.title,
      subTitle: data.subTitle,
      buttons: [{
        text: 'Ok', handler: (data) => {
          if (setFocus) {
            setFocus.setFocus();
          }
        }
      }]
    });
    alert.present();
  }

  auth(): boolean {
    let logged = window.localStorage.getItem('Logged_In');
    if (logged === "true") {
      return true
    }
    return false;
  }

  loginSet(data) {
    window.localStorage.setItem("Logged_In", "true");
    window.localStorage.setItem("Member_ID", data.Member_ID);
    window.localStorage.setItem("Name", data.Name);
    window.localStorage.setItem("Phone", data.Phone);
    window.localStorage.setItem("Email", data.Email);
  }

  logoutSet() {
    window.localStorage.removeItem('Logged_In');
    window.localStorage.removeItem("Member_ID");
    window.localStorage.removeItem("Name");
    window.localStorage.removeItem("Phone");
    window.localStorage.removeItem("Email");
  }

  checkValue(value: any): boolean {
    if (value == null || value == "" || value == undefined || value == "undefined") {
      return false;
    }
    return true;
  }


  autoToggleToolbar(ionScroll, showheader, hideheader, myElement) {
    let start = 0;
    let threshold = 50;

    ionScroll = myElement.getElementsByClassName('scroll-content')[0];
    ionScroll.addEventListener("scroll", () => {
      if (ionScroll.scrollTop - start > threshold) {
        showheader = true;
        hideheader = false;
      } else {
        showheader = false;
        hideheader = true;
      }
    });
  }

  substring(value: any, start: number, end: number): any {
    return value.substring(start, end);
  }






}
