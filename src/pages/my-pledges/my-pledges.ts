import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { PledgesPage } from '../pledges/pledges';
import { ModelService } from '../../providers/model.service';
import { MyPledgeDetailsPage } from '../my-pledge-details/my-pledge-details';

/**
 * Generated class for the MyPledgesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-pledges',
  templateUrl: 'my-pledges.html',
})
export class MyPledgesPage {
  pledges_count: number;
  pledges: any;
  member_id: string;
  pushPledges: any;

  constructor(
    public navCtrl: NavController, 
    public loadingCtrl: LoadingController,
    public modelService: ModelService,
    public navParams: NavParams) {
      this.member_id = window.localStorage.getItem("Member_ID");
      this.pushPledges = PledgesPage;

  }

  ionViewDidLoad() {
    this.get_pledges(this.member_id);
  }


  get_pledges(member_id){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.modelService.my_pledges(member_id).subscribe(data => {
      if (data) {
        if (data.response == "Success") {
          this.pledges = data.details;
          this.pledges_count = this.pledges.length;
          console.log(this.pledges.length, 'total');
          loading.dismiss();
        } else {
          console.error('error 1');
        }
      } else {
        console.error('error 2');
      }
    },
      error => {
        console.log('error 3');
        console.log(error, 'status: get_pledges');
      });
  }


  pushDetails(pledge){
    this.navCtrl.push(MyPledgeDetailsPage, {
      pledge_id : pledge.pledge_id
    });
  }

}
