import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyPledgesPage } from './my-pledges';

@NgModule({
  declarations: [
    MyPledgesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyPledgesPage),
  ],
})
export class MyPledgesPageModule {}
