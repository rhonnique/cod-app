import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
declare var PaystackPop: any;

/**
 * Generated class for the MyPledgeDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-pledge-details',
  templateUrl: 'my-pledge-details.html',
})
export class MyPledgeDetailsPage {
  
  
  transactionStatusMsg: any;
  date: any;
  total: any;
  postAmount: any;
  commission: any;
  paystackKey: any;
  ref: any;
  pledgeForm: FormGroup;
  amount_pending: number;
  amount_paid: any;
  pledged_amount: any;
  member_id: string;
  pledge: any;
  pledgeID: any;
  amount: any;

  viewDetails: boolean = true;
  viewPledge: boolean = false;
  viewSummary: boolean = false;
  summaryDone: boolean = false;
  showError: boolean;

  constructor(
    public navCtrl: NavController, 
    public loadingCtrl: LoadingController,
    public modelService: ModelService,
    public fb: FormBuilder,
    public navParams: NavParams,
    private elementRef: ElementRef) {
      this.member_id = window.localStorage.getItem("Member_ID");
      this.pledgeID = navParams.get('pledge_id');

      this.pledgeForm = fb.group({
        amount: [null, Validators.required]
      });
  }

  ionViewDidLoad() {
    this.get_pledge_details(this.member_id);

    var ps = document.createElement("script");
    ps.type = "text/javascript";
    ps.src = "https://js.paystack.co/v1/inline.js";
    this.elementRef.nativeElement.appendChild(ps);

  }


  get_pledge_details(member_id){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.modelService.my_pledge_details(member_id, this.pledgeID).subscribe(data => {
      if (data) {
        if (data.response == "Success") {
          this.pledge = data.details;
          this.pledged_amount = this.pledge.pledged_amount;
          this.amount_paid = this.pledge.amount_paid == null ? 0.00 : this.pledge.amount_paid;
          this.amount_pending = parseFloat(this.pledged_amount) - parseFloat(this.amount_paid); 

          loading.dismiss();
        } else {
          
        }
      } else {

      }
    },
      error => {

      });
  }

 
  

  summary(post: any) {
    post['member_id'] = window.localStorage.getItem("Member_ID");
     
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.modelService.pledge_summary(post).subscribe(data => {
      loading.dismiss();
      if (data) {
        if (data.response == "Success") {

          this.ref = data.details.Ref;
          this.paystackKey = data.details.Paystack_Key;
          this.commission = data.details.Commission;
          this.postAmount = data.details.Post_Amount;
          this.total = data.details.Total;

          this._viewSummary()

        } else {

        }
      } else {

      }
    },
      error => {
        //loading.dismiss();
      });
      
    /**/
  }



  payStack() {
    try {
      let ref = this.ref
      var handler = PaystackPop.setup({
        key: this.paystackKey,
        email: window.localStorage.getItem("Email"),
        amount: this.postAmount,
        ref: ref,
        meta:[{ flightid:3849 }],
        callback: (response) => {
          console.log('success. transaction ref is ' + response.reference);
          this.completeTransaction(response.reference);
        },
        onClose: function () {
          console.log('window closed');
        }
      });
      handler.openIframe();
    } catch (e) {
      // No content response..
      console.log(e);
    }
  }



  completeTransaction(ref) {
    let data = {};
    data['member_id'] = window.localStorage.getItem("Member_ID");
    data['pledge_id'] = this.pledgeID;
    data['ref'] = ref;
    data['amount'] = this.amount;
    data['commission'] = this.commission;
    console.log(data);
    /**/
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
    
        loading.present();

    this.modelService.pledge_done(data).subscribe(data => {
      loading.dismiss();
      if (data) {
        if (data.response == "Success") {
          console.log(data.details);
          this.ref = ref;
          this.commission = this.commission;
          this.total = data.details.Total;
          this.date = data.details.Date;
          this.transactionStatusMsg = data.details.Status_Msg;
          this.summaryDone = true;

        } else {
          console.error('error 1');
        }
      } else {
        console.error('error 2');
      }
    },
      error => {
        loading.dismiss();
        this._errorView();
      });
  }


  _errorView(){
    this.viewDetails = false;
    this.viewPledge = false;
    this.viewSummary = false;
    this.summaryDone = false;
    this.showError = true;
  }

  _reset(){
    this.viewDetails = true;
    this.viewPledge = false;
    this.viewSummary = false;
    this.summaryDone = false;
    this.showError = false;

    this.transactionStatusMsg = '';
    this.date = '';
    this.total = '';
    this.postAmount = '';
    this.commission = '';
    this.paystackKey = '';
    this.ref = '';
    this.amount = '';
  }

  _viewDetails(){
    this.viewDetails = true;
    this.viewPledge = false;
    this.viewSummary = false;
  }

  _viewPledge(){
    this.viewDetails = false;
    this.viewPledge = true;
    this.viewSummary = false;
  }

  _viewSummary(){
    this.viewDetails = false;
    this.viewPledge = false;
    this.viewSummary = true;
  }

}
