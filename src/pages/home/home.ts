import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { NavController, Content, Nav } from 'ionic-angular';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
//import { Page1Page } from './Page1Page';
//import { Page2Page } from './Page2Page';
import { SuperTabsController } from 'ionic2-super-tabs';
//import { TabAllPage } from '../tab-all/tab-all';
import { ModelService } from '../../providers/model.service';
import { PodcastPage } from '../podcast/podcast';
import { BulletinsPage } from '../bulletins/bulletins';
import { PledgesPage } from '../pledges/pledges';
import { LiveStreamingPage } from '../live-streaming/live-streaming';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pagePodCast: any;
  pageBulletin: any;
  pagePledges: any;
  pageLiveStream: any;

  /** Toggle Toolbar */
  start = 0;
  threshold = 50;
  ionScroll: any;
  showheader: boolean = false;
  hideheader: boolean = true;
  toggleTitle: boolean = false;
  /** Toggle Toolbar Ends */

  //page1: any = Page1Page;
  //page2: any = Page2Page;
  //page3: any = TabAllPage;

  constructor(public navCtrl: NavController, public renderer: Renderer,
    public elementRef: ElementRef,
    public modelService: ModelService,
    private superTabsCtrl: SuperTabsController,
    private nav: Nav) {
    
      this.pagePodCast = PodcastPage;
      this.pageBulletin = BulletinsPage;
      this.pagePledges = PledgesPage;
      this.pageLiveStream = LiveStreamingPage;
  }

  ngAfterViewInit() {

    // must wait for AfterViewInit if you want to modify the tabs instantly
    //this.superTabsCtrl.setBadge('homeTab', 5);

  }

  onTabSelect(ev: any) {
    console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
  }

  ngOnInit() {
    /** Toggle Toolbar */
    this.ionScroll = this.elementRef.nativeElement.getElementsByClassName('scroll-content')[0];
    this.ionScroll.addEventListener("scroll", () => {
      if (this.ionScroll.scrollTop - this.start > this.threshold) {
        this.showheader = true;
        this.hideheader = false;
        this.toggleTitle = true;
      } else {
        this.showheader = false;
        this.hideheader = true;
        this.toggleTitle = false;
      }
    });
    /** Toggle Toolbar Ends */
  }

  loadPage(page) {
    this.nav.setRoot(page);
  }



}
