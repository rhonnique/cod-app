import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, App, Events } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ModelService } from '../../providers/model.service';
import { DonationsPage } from '../donations/donations';
import { HomePage } from '../home/home';
import { PledgeDetailsPage } from '../pledge-details/pledge-details';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  pledge_id: any;
  donation_type_others: any;
  donation_type: any;
  return: any;
  donation_amount: any;
  push:any;
  loginForm: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public fb: FormBuilder,
    public loadingCtrl: LoadingController,
    public modelService: ModelService,
    public appCtrl: App,
    public events: Events) {

      this.return = navParams.get('return');
      this.donation_amount = navParams.get('donation_amount');
      this.donation_type = navParams.get('donation_type');
      this.donation_type_others = navParams.get('donation_type_others');

      this.pledge_id = navParams.get('pledge_id');
      
      this.loginForm = fb.group({
        email: [null, Validators.compose([Validators.required, Validators.email])],
        password: [null, Validators.required]
      });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad LoginPage');
  }


  login(data: any) {
    let pustData = {};
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.modelService.login(data).subscribe(data => {
      loading.dismiss();
      if (data) {
        if (data.response == "Success") {
          /* Store user data */
          this.modelService.loginSet(data.details);

          switch(this.return){
            case 'DonationsPage':
            pustData['donation_amount'] = this.donation_amount;
            pustData['donation_type'] = this.donation_type;
            pustData['donation_type_others'] = this.donation_type_others;
            this.push = DonationsPage;
            break;

            case 'PledgeDetailsPage':
            this.push = PledgeDetailsPage;
            pustData['pledge_id'] = this.pledge_id;
            break;

            default:
            this.push = HomePage;
          }

          this.events.publish('user:login', data.details);
          this.navCtrl.setRoot(this.push, pustData);

        } else {
          this.modelService.dfAlert({
            title: "Login Error",
            subTitle: data.details.Msg + '!'
          });
        }
      } else {
        this.modelService.dfAlert({
          title: "Login Error",
          subTitle: 'Can not log you in, please retry!'
        });
      }
    },
      error => {
        loading.dismiss();
        this.modelService.dfAlert({
          title: "Login Error",
          subTitle: 'Can not login you, please retry!'
        });
      });


      
  }



}
