import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';

/**
 * Generated class for the BulletinsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bulletins',
  templateUrl: 'bulletins.html',
})
export class BulletinsPage {
  bulletin: any;
  checked: boolean = false;
  total: number = 0;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modelService: ModelService,
    public loadingCtrl: LoadingController,
    private elementRef: ElementRef) {
  }


  ngOnInit() {
  }

  ionViewDidLoad() {
    this.get_bulletin();
  }



  get_bulletin(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    loading.present();

    this.modelService.bulletin().subscribe(data => {
      loading.dismiss();
      // console.log(data, 'resssss');
      if (data) { 
        if (data.response == "Success") {
          this.checked = false;
          this.total = 1;
          this.bulletin = data.details;
        } else {
          this.checked = true;
          this.total = 0;
        }
      } else {
        this.checked = true;
        this.total = 0;
      }
    },
      error => {
        loading.dismiss();
        this.checked = true;
        this.total = 0;
      });
  }



}
