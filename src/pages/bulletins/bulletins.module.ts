import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BulletinsPage } from './bulletins';

@NgModule({
  declarations: [
    BulletinsPage,
  ],
  imports: [
    IonicPageModule.forChild(BulletinsPage),
  ],
})
export class BulletinsPageModule {}
