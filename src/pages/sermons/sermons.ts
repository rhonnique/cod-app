import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Platform } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';

import { YoutubeServiceProvider } from '../../providers/youtube-service/youtube-service';
import { Http } from '@angular/http';
//import { PlayerProvider } from '../../providers/player/player';

import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

import { YtProvider } from './../../providers/yt/yt';
import { Observable } from 'rxjs/Observable';
import { SermonDetailsPage } from '../sermon-details/sermon-details';

/**
 * Generated class for the SermonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sermons',
  templateUrl: 'sermons.html',
  providers:[YoutubeServiceProvider]
})

export class SermonsPage {
  
  /** Toggle Toolbar */
  start = 0;
  threshold = 50;
  ionScroll: any;
  showheader: boolean = false;
  hideheader: boolean = true;
  toggleTitle: boolean = false;
  pageTitle = "Sermons";
  /** Toggle Toolbar Ends */

  channelID: string = 'UCo6uyrnKYeeMZssK8fAlW8Q';
  maxResults: string = '10';
  pageToken: string; 
  googleToken: string = 'AIzaSyANe3TV0ShD8MQmG_hyXB5v8pgoHnyTWNE';
  searchQuery: string = '';
  posts: any = [];
  onPlaying: boolean = false; 

  channelId = 'UCo6uyrnKYeeMZssK8fAlW8Q'; // Devdactic Channel ID
  playlists: Observable<any[]>;

  videos: Observable<any[]>;
  
  constructor(
    public navCtrl: NavController,
    public http: Http, 
    public nav:NavController, 
    private elementRef: ElementRef,
    public ytPlayer: YoutubeServiceProvider,
    private youtube: YoutubeVideoPlayer, 
    private ytProvider: YtProvider, 
    private alertCtrl: AlertController,
    private plt: Platform) {
  }

  ngOnInit() {
    /** Toggle Toolbar */
    this.ionScroll = this.elementRef.nativeElement.getElementsByClassName('scroll-content')[0];
    this.ionScroll.addEventListener("scroll", () => {
      if (this.ionScroll.scrollTop - this.start > this.threshold) {
        this.showheader = true;
        this.hideheader = false;
        this.toggleTitle = true;
      } else {
        this.showheader = false;
        this.hideheader = true;
        this.toggleTitle = false;
      }
    });
    /** Toggle Toolbar Ends */
  }


  ionViewDidLoad() {
    //this.youtube.openVideo('WSg88lGpLXk');
    this.getVideos('PLUMP1EBixvvwRm0XZkV8gMR6yWJOY7yfW');
  }


  getVideos(listId) {
    this.videos = this.ytProvider.getListVideos(listId);
    this.videos.subscribe(data => {
      console.log('playlists: ', data);
    }, err => {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'No Playlists found for that Channel ID',
        buttons: ['OK']
      });
      alert.present();
    })
  }

  openVideo(video) {
    if (this.plt.is('cordova')) {
      this.youtube.openVideo(video.snippet.resourceId.videoId);
    } else {
      window.open('https://www.youtube.com/watch?v=' + video.snippet.resourceId.videoId);
    }
  }

  searchPlaylists() {
    this.playlists = this.ytProvider.getPlaylistsForChannel(this.channelId);
    this.playlists.subscribe(data => {
      console.log('playlists: ', data);
    }, err => {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'No Playlists found for that Channel ID',
        buttons: ['OK']
      });
      alert.present();
    })
  }
 
  openPlaylist(id) { //PLUMP1EBixvvwRm0XZkV8gMR6yWJOY7yfW
    //console.log('list id: ', id);
    this.navCtrl.push(SermonDetailsPage, {id: id});
  }

  


}
