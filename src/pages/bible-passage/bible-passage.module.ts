import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BiblePassagePage } from './bible-passage';
import { NgCalendarModule  } from 'ionic2-calendar';

@NgModule({
  declarations: [
    BiblePassagePage,
  ],
  imports: [
    IonicPageModule.forChild(BiblePassagePage),
    NgCalendarModule
  ],
})
export class BiblePassagePageModule {}
