import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AboutServicesPage } from '../about-services/about-services';
import { AboutStatementPage } from '../about-statement/about-statement';
import { AboutContactPage } from '../about-contact/about-contact';
import { AboutVisionPage } from '../about-vision/about-vision';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  pageService = AboutServicesPage;
  pageStatement = AboutStatementPage;
  pageContact = AboutContactPage;
  pageVision = AboutVisionPage;

  /** Toggle Toolbar */
  start = 0;
  threshold = 50;
  ionScroll: any;
  showheader: boolean;
  hideheader: boolean;
  toggleTitle: boolean = false;
  pageTitle = "About The Church";
   /** Toggle Toolbar Ends */

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private elementRef: ElementRef,
    public modalCtrl: ModalController) {
      
  }


  ngOnInit() {
    /** Toggle Toolbar */
    this.ionScroll = this.elementRef.nativeElement.getElementsByClassName('scroll-content')[0];
    this.ionScroll.addEventListener("scroll", () => {
      if (this.ionScroll.scrollTop - this.start > this.threshold) {
        this.showheader = true;
        this.hideheader = false;
        this.toggleTitle = true;
      } else {
        this.showheader = false;
        this.hideheader = true;
        this.toggleTitle = false;
      }
    });
    /** Toggle Toolbar Ends */
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

  openModal(Page) {
    let modal = this.modalCtrl.create(Page);
    modal.present();
  }

  

}
