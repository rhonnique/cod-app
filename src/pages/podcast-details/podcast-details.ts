import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';
import { AudioProvider } from 'ionic-audio';

/**
 * Generated class for the PodcastDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-podcast-details',
  templateUrl: 'podcast-details.html',
})
export class PodcastDetailsPage {

  /** Toggle Toolbar */
  start = 0;
  threshold = 50;
  ionScroll: any;
  showheader: boolean;
  hideheader: boolean;
  toggleTitle: boolean = false;
  pageTitle = "Donations";
  /** Toggle Toolbar Ends */


  podcast_id: any;
  content_link: any;
  banner: any;
  author: any;
  sermonDetails: any;
  title: any;

  myTracks: any[];
  singleTrack: any;
  allTracks: any[];
  selectedTrack: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modelService: ModelService,
    public loadingCtrl: LoadingController,
    private _audioProvider: AudioProvider) {

    this.podcast_id = navParams.get('podcast_id');
    this.title = navParams.get('title');
    this.content_link = navParams.get('content_link');
    this.banner = navParams.get('banner');
    this.author = navParams.get('author');

    
    this.singleTrack = {
      src: this.content_link,
      artist: this.author,
      title: this.title,
      art: this.banner,
      preload: 'metadata'
    };
    
/*
    this.myTracks = [{
      src: this.content_link,
      artist: this.author,
      title: this.title,
      art: this.banner,
      preload: 'metadata'
    }];
    */

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PodcastDetailsPage');
  }

  ngAfterContentInit() { 
    //this.allTracks = this._audioProvider.tracks;
}

}
