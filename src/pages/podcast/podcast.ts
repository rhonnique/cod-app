import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';
import { PodcastDetailsPage } from '../podcast-details/podcast-details';




/**
 * Generated class for the PodcastPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-podcast',
  templateUrl: 'podcast.html',
})
export class PodcastPage {



   /** Toggle Toolbar */
   start = 0;
   threshold = 50;
   ionScroll: any;
   showheader: boolean;
   hideheader: boolean;
   toggleTitle: boolean = false;
   pageTitle = "Podcast";
    /** Toggle Toolbar Ends */


  content_link: any;
  banner: any;
  author: any;
  sermonDetails: any;
  title: any;
  sermonID: any;
  singleTrack: any;
  allTracks: any[];
  selectedTrack: number;
  myTracks: any[];

  podcasts: any[];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public modelService: ModelService,
    private elementRef: ElementRef,
    public loadingCtrl: LoadingController) {
      //this.getPodcast();
  }


  ngOnInit() {
    /** Toggle Toolbar */
    this.ionScroll = this.elementRef.nativeElement.getElementsByClassName('scroll-content')[0];
    this.ionScroll.addEventListener("scroll", () => {
      if (this.ionScroll.scrollTop - this.start > this.threshold) {
        this.showheader = true;
        this.hideheader = false;
        this.toggleTitle = true;
      } else {
        this.showheader = false;
        this.hideheader = true;
        this.toggleTitle = false;
      }
    });
    /** Toggle Toolbar Ends */
  }


  ionViewDidLoad() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    setTimeout(() => {
      loading.dismiss();
    }, 5000);
  }

  getPodcast(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.modelService.podcast().subscribe(data => {
      if (data) {
        if (data.response == "Success") {
          console.log(data.details);
          this.podcasts = data.details;
          loading.dismiss();
        } else {
          
        }
      } else {

      }
    },
      error => {

      });
  }

  pushDetails(podcast){
    console.log(podcast,'podcast data:');
    this.navCtrl.push(PodcastDetailsPage, {
      podcast_id : podcast.podcast_id,
      author : podcast.author,
      title : podcast.title,
      content_link : podcast.content_link,
      banner : podcast.banner
    });
  }

}
