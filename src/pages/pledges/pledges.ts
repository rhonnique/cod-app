import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ModelService } from '../../providers/model.service';
import { PledgeDetailsPage } from '../pledge-details/pledge-details';

/**
 * Generated class for the PledgesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pledges',
  templateUrl: 'pledges.html',
})
export class PledgesPage {
  pledgeDetails: any;
  pledges: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modelService: ModelService) {
  }

  ionViewDidLoad() {
    this.getPledges();
  }


  getPledges(){
    this.modelService.pledges().subscribe(data => {
      if (data) { 
        if (data.response == "Success") {
          this.pledges = data.details;
          //console.log(this.pledges)
        } else {

        }
      } else {

      }
    },
      error => {

      });
  }

  pushDetails(pledge){
    this.navCtrl.push(PledgeDetailsPage, {
      pledge_id: pledge.pledge_id,
      title: pledge.name
    });
  }


}
